var pup1, pup2, creep, creepContainer, anchor1, off, eye1, eye2;
const offset1 = {
    x: 217,
    y: 332
};

const offset2 = {
    x: 345,
    y: 333
};

const maxDist = 300;

function setAnchor() {
    anchor1 = {
        x: creepContainer.position().left + creep.position().left + offset1.x,
        y: creepContainer.position().top + creep.position().top + offset1.y
    };

    anchor2 = {
        x: creepContainer.position().left + creep.position().left + offset2.x,
        y: creepContainer.position().top + creep.position().top + offset2.y
    };
}

function placeEyes() {
    setAnchor();

    pup1.css('left', anchor1.x - off);
    pup1.css('top', anchor1.y - off);

    pup2.css('left', anchor2.x - off);
    pup2.css('top', anchor2.y - off);

    eye1.css('left', anchor1.x - eye1.width()/2);
    eye1.css('top', anchor1.y - eye1.height()/2);

    eye2.css('left', anchor2.x - eye2.width()/2 - 5);
    eye2.css('top', anchor2.y - eye2.height()/2);
}

$(window).on('resize', placeEyes);

$(document).ready(() => {
    pup1 = $('<div class="pupil">');
    pup2 = $('<div class="pupil">');
    $('body').append(pup1);
    $('body').append(pup2);

    eye1 = $('<div class="eye">');
    $('body').append(eye1);
    eye2 = $('<div class="eye">');
    $('body').append(eye2);

    creep = $('#creep');
    creepContainer = $('#creepContainer');

    off = pup1.height() / 2;
    placeEyes();
    setTimeout(placeEyes, 100);

    $(document).on('mousemove', moveEyes);
    document.addEventListener('touchmove', moveEyes, {passive: false});
    // $(document).on('touchmove', moveEyes);
});

function moveEyes(event) {
    setAnchor();
    event.preventDefault();
    var position = {};
    if(typeof event.pageX !== "undefined") {
        position.x = event.pageX;
        position.y = event.pageY;
    } else {
        position.x = event.touches[0].pageX;
        position.y = event.touches[0].pageY;
    }

    var a1 = anchor1, a2 = anchor2;
    var m = position;

    const alpha1 = calcAngle(a1, m)/60;
    const dis1 = calcDistance(a1, m);
    const fac1 = (dis1 >= maxDist ? 1 : dis1/maxDist);

    var p1 = {
        x: a1.x + fac1 * 17 * Math.cos(alpha1) - off,
        y: a1.y - fac1 * 13.5 * Math.sin(alpha1) - off
    }


    pup1.css('left', p1.x);
    pup1.css('top', p1.y);

    const alpha2 = calcAngle(a2, m)/60;
    const dis2 = calcDistance(a2, m);
    const fac2 = (dis2 >= maxDist ? 1 : dis2/maxDist);

    var p2 = {
        x: a2.x + fac2 * 17 * Math.cos(alpha2) - off,
        y: a2.y - fac2 * 13.5 * Math.sin(alpha2) - off
    }


    pup2.css('left', p2.x);
    pup2.css('top', p2.y);
}

/**
 * 
 * @param {*} p1 Anchor point
 * @param {*} p2 Mouse point
 */
function calcAngle(p1, p2) {
    const deltaX = p2.x - p1.x;
    const deltaY = p1.y - p2.y;
    return Math.atan2(deltaY, deltaX) * 180 / Math.PI;
}

function calcDistance(p1, p2) {
    return Math.sqrt( Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2) );
}